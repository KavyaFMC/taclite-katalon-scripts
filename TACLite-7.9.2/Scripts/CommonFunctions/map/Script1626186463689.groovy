import java.util.Arrays
import java.util.List
import java.util.Map
import java.util.function.Function
import java.util.stream.Collectors

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.io.IOException
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable

import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import excelHandle.excelValues as excelValues
import excelHandle.excelGetValues as excelGetValues
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testdata.InternalData
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence
import java.sql.Driver
import java.util.concurrent.TimeUnit

import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence
import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver as WebDriver

import org.apache.commons.lang3.StringUtils

import com.kms.katalon.core.testdata.reader.ExcelFactory

import java.lang.*;
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
// Program to count the frequency of the elements in a list

fetchValues("C://Users//z00444364//Book1.xlsx")

public fetchValues(String Path){
	
FileInputStream fis = new FileInputStream(Path)
XSSFWorkbook workbook = new XSSFWorkbook(fis)

XSSFSheet sheet = workbook.getSheet("Sheet1")
int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum()
println(rowCount)

int colCount = sheet.getRow(1).getLastCellNum()
println(colCount)
//Row row = sheet.createRow(rowCount+1);
Row row =sheet.getRow(1)
//println(row)
int i
List<String> list = new ArrayList()
for (i=0;i<colCount;i++) {

Cell cell = row.getCell(i)

println(cell)

list.add(cell);
println(list)


Map<String, Integer> frequencyMap = new HashMap<>();
for (String s: list)
{
	Integer count = frequencyMap.get(s);
	if (count == null) {
		count = 0;
	}

	frequencyMap.put(s, count + 1);
	

}

for (Map.Entry<String, Integer> entry: frequencyMap.entrySet()) {
	System.out.println(entry.getKey() + ": " + entry.getValue());
	/*String key = entry.getKey();
	if(key.contains("Passed")){
		System.out.println(key + "/" + entry.getValue());
}
else {
	
}*/
}
}
}






/*
 Map<String,Long> occurrenceMap =
                list.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
        System.out.println("Count: " + occurrenceMap);
*/




/*
def a ="Passed"
def b ="Failed"
String value = cell
//cell=cell.trim()
//cell=cell.trim()
println(a)

if(WebUI.verifyEqual(a,value, FailureHandling.OPTIONAL)) {
	
	int count=0;
	count = count++;
	println("Passed count:"+count)
}
else if(WebUI.verifyMatch(b,value, FailureHandling.OPTIONAL)) {
	int count=0
count = count++;
println("Failed count:"+count)
}
else
{
	int count=0;
count = count++;
println("Not Executed count:"+count)
}


}
}

/*
if (WebUI.verifyEqual(a, cell, FailureHandling.OPTIONAL)) {
int count
	count = count++;
	println("Passed count:"+count)
}
else if(WebUI.verifyMatch(b, cell, FailureHandling.OPTIONAL)) {
	int count
count = count++;
println("Failed count:"+count)
}
else
{
	int count
count = count++;
println("Not Executed count:"+count)
}

}
}



/*

if(WebUI.verifyMatch(a, cell, FailureHandling.OPTIONAL)) {
	
	int count
	count = count++;
	println("Passed count:"+count)
	}

else if(WebUI.verifyMatch(b, cell, FailureHandling.OPTIONAL)) {
		int count
	count = count++;
	println("Failed count:"+count)
}
else
{
		int count
	count = count++;
	println("Not Executed count:"+count)
}
}
}
/*
list.add(cell);
println(list)


List<String> list = new ArrayList();


list.add(cell);

println(list.add(cell))

println(list)





//println(''+list.get())
}

Map<String, Long> frequencyMap =
list.stream().collect(Collectors.groupingBy(Function.identity(),
										Collectors.counting()));
									
for (Map.Entry<String, Long> entry: frequencyMap.entrySet()) {
println(""+entry.getKey()+" : "+entry.getValue())



	// + ": " + entry.getValue());


for(String string : list) {
	System.out.println(list)
}



}
}



//sheet.add(list);


        Map<String, Long> frequencyMap =
            list.stream().collect(Collectors.groupingBy(Function.identity(),
                                                    Collectors.counting()));
 
        for (Map.Entry<String, String> entry: frequencyMap.entrySet()) {
            //System.out.println(entry.getKey() + ": " + entry.getValue());
			
			//println(+entry.getKey() + ": " + entry.getValue())
        }
*/
    
