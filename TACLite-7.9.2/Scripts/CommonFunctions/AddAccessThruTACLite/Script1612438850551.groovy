import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.io.IOException as IOException
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import excelHandle.excelValues as excelValues
import excelHandle.excelGetValues as excelGetValues
import com.kms.katalon.core.annotation.Keyword as Keyword
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.testdata.InternalData as InternalData
import java.io.FileInputStream as FileInputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.FileOutputStream as FileOutputStream
import org.openqa.selenium.By as By
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import java.time.LocalDate as LocalDate
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.time.LocalDateTime;

Properties prop = supportingfiles.SupportingFiles.importPropertiesFile('Add_Access')
Properties prop1 = supportingfiles.SupportingFiles.importPropertiesFile('Environmental_Variables')

//Properties prop = helperUtilities.getTheObject('C://katalon//TACLite//Data Resources//TacLite_Properties//AddAccess.properties')

//Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')

WebUI.takeScreenshot()

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')

//CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Addnewaccess'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('CVCAdd'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AccLocation'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AccStatus'), '', 'click')

//Date
DateTimeFormatter dtformatter = DateTimeFormatter.ofPattern('MM/dd/yyyy')

//String strShiftDate = shiftDate

//LocalDate ldShift = LocalDate.parse(strShiftDate, dtformatter)

//numberofweek = Integer.parseInt(testData.get('NumberOfWeeksPrevious'))

/*LocalDate ldMaturation = ldShift.minusWeeks(numberofweek)

LocalDate cdate = ldMaturation.minusDays(1)

println(ldMaturation.getYear())

println(ldMaturation.getMonthValue())

println(ldMaturation.getDayOfMonth())

println(cdate.getDayOfMonth())

date = ldMaturation.getDayOfMonth().toString()

month = ldMaturation.getMonthValue().toString()

year = ldMaturation.getYear().toString()
*/

  
   LocalDate now = LocalDate.now();  
   System.out.println(dtformatter.format(now)); 
   date=dtformatter.format(now)
creationdate = now.getDayOfMonth().toString()

month = now.getMonthValue().toString()

year = now.getYear().toString()

System.out.println(creationdate);
CustomKeywords.'reusableComponents.DatepickerHandler.handleDatepicker'(prop.getProperty('DateStatus'), date,
	date, date)

//CustomKeywords.'reusableComponents.DatepickerHandler.handleDatepicker'(findTestObject('AddAccess/btnCreationDate'), creationdate,
	//month, year)


//------------------------------------------------------------------------



CustomKeywords.'reusablekeyword.Reusable.datepicker'(prop.getProperty('AddStatusDate'))

CustomKeywords.'reusablekeyword.Reusable.datepicker'(prop.getProperty('AddCreationDate'))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AddUsedTodayYes'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AddLineSourceBoth'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

