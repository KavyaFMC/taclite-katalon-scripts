import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.Keys as Keys
import java.util.List as List
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import org.stringtemplate.v4.compiler.STParser.element_return as element_return
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.firefox.FirefoxDriver as FirefoxDriver
import org.openqa.selenium.*
//import org.openqa.selenium.SearchContext.findElements
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By.ByXPath as ByXPath
import utilityKeywords.helperUtilities as helperUtilities

WebUI.comment('Load the required property files')

Properties propNx2 = supportingfiles.SupportingFiles.importPropertiesFile('NxStage_MachineSetup')
Properties prop1 = supportingfiles.SupportingFiles.importPropertiesFile('Patient_Selection')
Properties propNx = supportingfiles.SupportingFiles.importPropertiesFile('NxStage_PropertyValues')
Properties propNx1 = supportingfiles.SupportingFiles.importPropertiesFile('NxStage_EnvVariables')

//Properties propNx2 = helperUtilities.getNxProp3('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_MachineSetup.properties')
//Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//PatientSelection.properties')
//Properties propNx = helperUtilities.getNxProp1('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_PropertyValues.properties')
//Properties propNx1 = helperUtilities.getNxProp2('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_EnvVariables.properties')

WebUI.comment('CAR/Non CAR Orders-Machine Setup script execution based on the condition check')
CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('CarLotNo'), '', 'conditioncheck')



