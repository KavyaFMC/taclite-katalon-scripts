package pageLocators

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling

import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import com.kms.katalon.core.testdata.reader.ExcelFactory

public class pageOperation {

	@Keyword
	public void pageAction(String xpath,String value,String type){
		TestObject testObj=new TestObject()
		switch (type){

			case "input":
				testObj.addProperty("xpath", ConditionType.EQUALS, xpath)
				WebUI.setText(testObj,value);
				break;

			case "click":

				testObj.addProperty("xpath", ConditionType.EQUALS, xpath)
				WebUI.click(testObj);
				break;

			case "dropdown":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.selectOptionByLabel(testObj,value, false)
				break;

			case "switchframe":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.switchToFrame(testObj, 3)
				break;

			case "defaultcontent":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.switchToDefaultContent()
				break;

			case "cleardata":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.clearText(testObj)
				break;

			case "verifytext":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.verifyElementText(testObj, value)
				break;

			case "scrolltoelement":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.scrollToElement(testObj, 10)
				break;

			case "verifyelement":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
			//WebUI.verifyElementPresent(testObj, 10)

				WebUI.verifyElementPresent(testObj, 5, FailureHandling.OPTIONAL)

				break;

			case "mouseOver":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
			//WebUI.takeScreenshot()
			//WebUI.verifyElementClickable(testObj)
			//WebUI.verifyElementVisible(testObj)
				WebUI.mouseOver(testObj)
				break;

			case "choosemenu":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.selectOptionByValue(testObj, value, false)
				break;

			case "valueselection":

				testObj.addProperty("xpath",ConditionType.EQUALS, xpath)
				WebUI.getText(testObj)
				break;

			case "conditioncheck":

				def a=testObj.addProperty('xpath', ConditionType.EQUALS, xpath)
				if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)){
					println("Cartridge")
					WebUI.callTestCase(findTestCase('NxStageOrders/NonCARwithPureflow'), [:], FailureHandling.STOP_ON_FAILURE)
					WebUI.callTestCase(findTestCase('NxStageOrders/CS_HDOrders_Cartridge'), [:], FailureHandling.STOP_ON_FAILURE)
					WebUI.callTestCase(findTestCase('NxStageOrders/PhysicianView_Cartridge'), [:], FailureHandling.STOP_ON_FAILURE)
				}
				else{
					println("Dialyzer")
					WebUI.callTestCase(findTestCase('NxStageOrders/CARwithPureflow'), [:], FailureHandling.STOP_ON_FAILURE)
					WebUI.callTestCase(findTestCase('NxStageOrders/CS_HDOrders_Dialyzer'), [:], FailureHandling.STOP_ON_FAILURE)
					WebUI.callTestCase(findTestCase('NxStageOrders/PhysicianView_Dialyzer'), [:], FailureHandling.STOP_ON_FAILURE)

				}

			case "ifcheck":

				def b=testObj.addProperty('xpath', ConditionType.EQUALS, xpath)
				println(b)
				if (WebUI.waitForElementPresent(b, 3, FailureHandling.OPTIONAL)){

					println("if part")

				}
				else{
					println("else part")

				}

			case "check":

				def a=testObj.addProperty('xpath', ConditionType.EQUALS, xpath)
				if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)){


				}
				else{
					println("Not AVG or AVF access")
				}





		}
	}
}
