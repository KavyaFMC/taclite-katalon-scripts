package reusableComponents

import jxl.Cell
import jxl.CellType
import jxl.Sheet
import jxl.Workbook

public class DataReaderjexcel {

	String SheetName, xlname;
	File inputWorkbook;
	Workbook w;
	Sheet sheet;
	LinkedHashMap<String, Integer> mapColumns = new LinkedHashMap<String, Integer>();

	public DataReaderjexcel(String xl, String sht) {
		xlname = xl;
		SheetName = sht;

		try {
			inputWorkbook = new File("./InputData/" + xlname + ".xls");


			w = Workbook.getWorkbook(inputWorkbook);
			sheet = w.getSheet(SheetName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closeDataReader() {
		try {
			w.close();
			w = null;
			inputWorkbook = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public LinkedHashMap<String, Integer> getColumnMap() {
		int columncount = sheet.getColumns();

		for (int colNum = 0; colNum < columncount; colNum++) {
			String cellvalue="";
			Cell cell = sheet.getCell(colNum, 0);

			cellvalue = cell.getContents();
			mapColumns.put(cellvalue, colNum);
		}

		return mapColumns;
	}

	public String[][] readxl() {
		String[][] data;

		int rowcount = sheet.getRows();
		int columncount = sheet.getColumns();
		data = new String[rowcount][columncount];

		for(int i=1;i<rowcount;i++) {

			for(int j=0;j<columncount;j++) {
				String cellvalue="";
				Cell cell = sheet.getCell(j, i);
				CellType type = cell.getType();

				if (type == CellType.LABEL) {
					cellvalue = cell.getContents();
				} else if (type == CellType.NUMBER) {
					cellvalue = "" + cell.getContents();
				} else if (type == CellType.EMPTY) {
					cellvalue = "";
				}

				data[i-1][j]=cellvalue;
			}
		}

		return data;
	}

	public int gettingRowCount() {

		int itration = sheet.getRows();
		return itration;
	}

	public int gettingColumnCount() {

		int itration = sheet.getColumns();
		return itration;
	}
}