import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.io.IOException
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testdata.InternalData
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

import java.lang.CharSequence as CharSequence
import java.sql.Driver
import java.util.concurrent.TimeUnit

import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.lang.CharSequence as CharSequence
import com.kms.katalon.core.testobject.ConditionType
import utilityKeywords.helperUtilities as helperUtilities
import reusablekeyword.Reusable as Reusable

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

writeTimeValues("C:\\katalon\\NxStage Orders\\InputData\\ReadWrite.xlsx")

public writeTimeValues(String Path){
	
	/*Properties prop = helperUtilities.getTheObject('C://katalon//NxStage Orders//Data Resources//NxStageOrders//NxStage_MachineSetup.properties')
	Properties prop1 = helperUtilities.getTheObj('C://katalon//NxStage Orders//Data Resources//ChairSide//CS_PatientSelection.properties')
	Properties prop2 = helperUtilities.getObj('C://katalon//NxStage Orders//Data Resources//NxStageOrders//NxStage_PropertyValues.properties')
	Properties prop3 = helperUtilities.getObj1('C://katalon//NxStage Orders//Data Resources//NxStageOrders//NxStage_EnvVariables.properties')
	Properties prop4 = helperUtilities.getObj2('C://katalon//NxStage Orders//Data Resources/ChairSide//CS_StaticValues.properties')
	*/
	Properties prop5 = helperUtilities.getObj3('C://katalon//NxStage Orders//Data Resources/ChairSide_Properties//PreDialysis.properties')

	
	Schedhours = 1
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm')
	
	println(formatter)
	
	LocalDateTime datetime = LocalDateTime.now()
	
	println(datetime)
	
	datetime = datetime.minusHours(Schedhours)
	
	String aftersubtraction = datetime.format(formatter)
	
	println('' + aftersubtraction)
	
	String[] preTime = aftersubtraction.split('\\ ')
	
	println('' + (preTime[1]))
	
	//String split_Time=preTime[intschedarr[0]];
	
	
	//String split_Time=preTime[intschedarr[0]];
	Time = ('' + (preTime[1]))
	

		FileInputStream fis = new FileInputStream(Path);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
	
		XSSFSheet sheet = workbook.getSheet("Data1");
		int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
		println(rowCount)
		Row row = sheet.createRow(rowCount+1);
		Cell cell = row.createCell(0);
		
		//Cell cell = row.getCell(1)
		cell.setCellType(cell.CELL_TYPE_STRING);
		cell.setCellValue(Time);
		FileOutputStream fos = new FileOutputStream(Path);
		workbook.write(fos);
		
		fos.close();
	}
	
	