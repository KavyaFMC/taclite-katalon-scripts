import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testobject.ConditionType

import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence
import java.sql.Driver
import java.util.concurrent.TimeUnit

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')
Properties prop2 = helperUtilities.getTheObject('C://katalon//TACLite//Data Resources//ChairSide.properties')

TestObject testObj = new TestObject()


def a = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Closebtn'))

if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)) {
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Closebtn'), '', 'click')
} else {
	println('Proceed to next-do nothing')
}

WebUI.takeScreenshot()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select1'), '', 'click')

WebUI.callTestCase(findTestCase('CommonFunctions/PostSurgical'), [:], FailureHandling.STOP_ON_FAILURE)

def b=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('PostSurgical'))

if (WebUI.waitForElementPresent(b, 3, FailureHandling.OPTIONAL)){
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PostSurgical'), '', 'click')
}
else{
	println("Proceed to next-do nothing")
}

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def PreTx_Look1=excelData.getValue('PreTx_Look1', 1)
println(PreTx_Look1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+PreTx_Look1+prop.getProperty('Pre'), '', 'click')

def PreTx_Listen1=excelData.getValue('PreTx_Listen1', 1)
println(PreTx_Listen1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+PreTx_Listen1+prop.getProperty('Pre'), '', 'click')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreOther'), '', 'scrolltoelement')

WebUI.delay(3)

def PreTx_Feel1=excelData.getValue('PreTx_Feel1', 1)
println(PreTx_Feel1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+PreTx_Feel1+prop.getProperty('Pre'), '', 'click')

def PreTx_Other1=excelData.getValue('PreTx_Other1', 1)
println(PreTx_Other1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+PreTx_Other1+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')
