import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import excelHandle.excelValues as excelValues
import excelHandle.excelGetValues as excelGetValues
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testdata.InternalData
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence
import java.sql.Driver
import java.util.concurrent.TimeUnit

import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence
import com.kms.katalon.core.testobject.ConditionType

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')
Properties prop2 = helperUtilities.getTheObject('C://katalon//TACLite//Data Resources//ChairSide.properties')

TestObject testObj=new TestObject()

/*
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'scrolltoelement')

WebUI.delay(3)
*/

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')
//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'click')
WebUI.delay(3)

WebUI.comment("Get the total number of 'Select' buttons on the page")

WebDriver driver = DriverFactory.getWebDriver()
driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS)
driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop2.getProperty('TAC_Frame'), '', 'switchframe')

List<WebElement> listBtn = driver.findElements(By.xpath("//button[contains(text(),'Select')]"))
			
a=listBtn.size()

WebUI.comment("Iterate through the for loop and click on the respective select buttons")

	for(int i = 1; i <= a; i++){

select="(//button[contains(text(),'Select')])"+"["+i+"]"

CustomKeywords.'pageLocators.pageOperation.pageAction'(select, '', 'click')

def e=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('cvcyesbutton'))
Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)

if(WebUI.waitForElementPresent(e, 1, FailureHandling.OPTIONAL)){
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('cvcyesbutton'), '', 'click')
	
	def PreTx_Other2=excelData1.getValue('PreTx_Other2', 2)
	println(PreTx_Other2)
		
	CustomKeywords.'pageLocators.pageOperation.pageAction'((prop.getProperty('Pre_Other1') + PreTx_Other2) + prop.getProperty(
		'Pre'), '', 'click')
	CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
	
	
	WebUI.delay(2)
	
	}

def b=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('PostSurgical'))

if (WebUI.waitForElementPresent(b, 1, FailureHandling.OPTIONAL)){
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PostSurgical'), '', 'click')
}
else{
	println("Proceed to next-do nothing")
}

def g=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Look_present'))

if (WebUI.waitForElementPresent(g, 1, FailureHandling.OPTIONAL)){


Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def PreTx_Look1=excelData.getValue('PreTx_Look1', 1)
println(PreTx_Look1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+PreTx_Look1+prop.getProperty('Pre'), '', 'click')

def PreTx_Listen1=excelData.getValue('PreTx_Listen1', 1)
println(PreTx_Listen1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+PreTx_Listen1+prop.getProperty('Pre'), '', 'click')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreOther'), '', 'scrolltoelement')

WebUI.delay(3)

def PreTx_Feel1=excelData.getValue('PreTx_Feel1', 1)
println(PreTx_Feel1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+PreTx_Feel1+prop.getProperty('Pre'), '', 'click')

def PreTx_Other1=excelData.getValue('PreTx_Other1', 1)
println(PreTx_Other1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+PreTx_Other1+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

WebUI.delay(2)

}



def c=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('pdnobutton'))

	if(WebUI.waitForElementPresent(c, 1, FailureHandling.OPTIONAL)){
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('pdnobutton'), '', 'click')
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PD_ok'), '', 'click')
	
	
	CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
	
	
	WebUI.delay(2)

	}
		

		
i+1
		
}
	
	

	