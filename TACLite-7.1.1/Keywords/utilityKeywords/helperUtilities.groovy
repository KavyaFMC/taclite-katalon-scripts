package utilityKeywords

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class helperUtilities {

	public static Properties getTheObject(String fileName) {

		File file = new File(fileName);
		FileInputStream fileInputStream=new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fileInputStream);
		return prop;
	}

	public static Properties getobject(String fileName) {

		File file = new File(fileName);
		FileInputStream fileInputStream=new FileInputStream(file);
		Properties staticvalues= new Properties();
		staticvalues.load(fileInputStream);
		return staticvalues;
	}

	public static Properties getTheObj(String fileName) {

		File file = new File(fileName);
		FileInputStream fileInputStream=new FileInputStream(file);
		Properties prop1 = new Properties();
		prop1.load(fileInputStream);
		prop1.get("Clinic");
		return prop1;
	}

	public static Properties getObj(String fileName) {
		File file = new File(fileName);
		FileInputStream fileInputStream=new FileInputStream(file);
		Properties prop2 = new Properties();
		prop2.load(fileInputStream);
		//prop2.get("Clinic");
		return prop2;
	}

	public static Properties getObj1(String fileName) {
		File file = new File(fileName);
		FileInputStream fileInputStream=new FileInputStream(file);
		Properties prop3 = new Properties();
		prop3.load(fileInputStream);
		//prop2.get("Clinic");
		return prop3;
	}

	public static Properties getObj2(String fileName) {
		File file = new File(fileName);
		FileInputStream fileInputStream=new FileInputStream(file);
		Properties prop4 = new Properties();
		prop4.load(fileInputStream);
		//prop2.get("Clinic");
		return prop4;
	}

	public static Properties getObj3(String fileName) {
		File file = new File(fileName);
		FileInputStream fileInputStream=new FileInputStream(file);
		Properties prop5 = new Properties();
		prop5.load(fileInputStream);
		//prop2.get("Clinic");
		return prop5;
	}

	public static Properties getNxProp1(String fileName) {
		File file = new File(fileName);
		FileInputStream fileInputStream=new FileInputStream(file);
		Properties propNx = new Properties();
		propNx.load(fileInputStream);
		//prop2.get("Clinic");
		return propNx;
	}

	public static Properties getNxProp2(String fileName) {
		File file = new File(fileName);
		FileInputStream fileInputStream=new FileInputStream(file);
		Properties propNx1 = new Properties();
		propNx1.load(fileInputStream);
		//prop2.get("Clinic");
		return propNx1;

	}

	public static Properties getNxProp3(String fileName) {
		File file = new File(fileName);
		FileInputStream fileInputStream=new FileInputStream(file);
		Properties propNx2 = new Properties();
		propNx2.load(fileInputStream);
		//prop2.get("Clinic");
		return propNx2;
	}
}
