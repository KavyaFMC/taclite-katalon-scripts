package sqlConnector

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class DBConnection {

	@Keyword
	public void connectionFHIR(String conn,String username,String password,String driverClassName) {

		Class.forName(driverClassName);
		Properties props = new Properties();
		props.setProperty("user",username);
		props.setProperty("password",password);
		props.setProperty("ssl","true");
		Connection connection = DriverManager.getConnection(conn,props)
		Statement stmt=connection.createStatement();
		println("Connection Done")
		String MRN='P5000555017'
		ResultSet rs=stmt.executeQuery("select * from stu3_cache.patient where SRC_FHIR_ID="+"'"+MRN+"'")

		while(rs.next()) {
			System.out.println(rs.getInt('ID')+" \t\t\t"+rs.getString('SRC_FHIR_ID'));
		}
		connection.close();
	}

	//fetch values from DB and compare it with excel inputs
	@Keyword
	public void connectionIhub(String conn,String username,String password,String driverClassName) {


		Class.forName(driverClassName);
		Properties props = new Properties();
		props.setProperty("user",username);
		props.setProperty("password",password);
		props.setProperty("ssl","true");
		Connection connection = DriverManager.getConnection(conn,props)
		Statement stmt=connection.createStatement();
		println("Connection Successful")



		String Occurance='B2147594213'
		//ResultSet rs=stmt.executeQuery("select * from stu3_cache.patient where SRC_FHIR_ID="+"'"+MRN+"'")
		ResultSet rs=stmt.executeQuery("SELECT * FROM CDS.HEMO_ORDER_OCCURRENCE where SC_ORDER_OCCURRENCE_ID="+"'"+Occurance+"'")

		//String status=rs.getString('CS_STATUS_CODE')
		while(rs.next()) {
			int i;
			for(i=0;i<4;i++)

				if(rs.getString('CS_STATUS_CODE')=='CM'){
					System.out.println("Completed");
					break


				}
				else {
					println("In Progress")
					Thread.sleep(60000)
				}

		}


		connection.close();
		println 'Connection Closed'
	}


	//fetch values from DB and compare it with excel inputs
	@Keyword
	public void dbExcel(String conn,String username,String password,String driverClassName) {


		Class.forName(driverClassName);
		Properties props = new Properties();
		props.setProperty("user",username);
		props.setProperty("password",password);
		props.setProperty("ssl","true");
		Connection connection = DriverManager.getConnection(conn,props)
		Statement stmt=connection.createStatement();
		println("Connection Done")
		String Occurance='B2147594213'
		//ResultSet rs=stmt.executeQuery("select * from stu3_cache.patient where SRC_FHIR_ID="+"'"+MRN+"'")
		ResultSet rs=stmt.executeQuery("select * from CHAIRSIDE.REF_SHIFT_STATUS where REF_ID = (Select SHIFT_STATUS_ID From Chairside.shift where shift_id = (Select shift_id From Chairside.Treatment where TREATMENT_ID = (Select TREATMENT_ID from CHAIRSIDE.HD_ORDER where OCCURANCE_ID ="+"'"+Occurance+"')))")
		println(''+rs.getString('CS_STATUS_CODE'))
		//String status=rs.getString('CS_STATUS_CODE')
		connection.close();
	}

	@Keyword
	public void writeExcel(String conn,String username,String password,String driverClassName){

		Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C:\\Users\\z00467714\\Documents\\DBExcel.xlsx', 'Data',true)
		int i;

		int rowCount=excelData.getRowNumbers()
		println(rowCount)
		for(i=1;i<=rowCount;i++){
			List elem=new ArrayList()
			elem[i-1] = excelData.getValue("OccID", i)


			println (elem[i-1])

			Class.forName(driverClassName);
			Properties props = new Properties();
			props.setProperty("user",username);
			props.setProperty("password",password);
			props.setProperty("ssl","true");
			Connection connection = DriverManager.getConnection(conn,props)
			Statement stmt=connection.createStatement();

			ResultSet rs=stmt.executeQuery("SELECT * FROM CDS.HEMO_ORDER_OCCURRENCE WHERE SC_ORDER_OCCURRENCE_ID="+"'"+elem[i-1]+"'")
			while(rs.next()){




				if(rs.getString('CS_STATUS_CODE')=='AC'){
					System.out.println("Active")
				}


				else{
					int j;

					for(j=0;j<4;j++){
						Thread.sleep(60000)
						System.out.println("active");

					}


				}
			}
			connection.close();




		}

	}

	@Keyword
	public void shiftStatusCheck(String conn,String username,String password,String driverClassName){

		Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C:\\Users\\z00467714\\Documents\\DBExcel.xlsx', 'Data',true)
		int i;

		int rowCount=excelData.getRowNumbers()
		println(rowCount)
		for(i=1;i<=rowCount;i++){
			List elem=new ArrayList()
			elem[i-1] = excelData.getValue("OccID", i)


			println (elem[i-1])

			Class.forName(driverClassName);
			Properties props = new Properties();
			props.setProperty("user",username);
			props.setProperty("password",password);
			props.setProperty("ssl","true");
			Connection connection = DriverManager.getConnection(conn,props)
			Statement stmt=connection.createStatement();

			ResultSet rs=stmt.executeQuery("select * from CHAIRSIDE.REF_SHIFT_STATUS where REF_ID=(Select SHIFT_STATUS_ID From Chairside.shift where shift_id = (Select shift_id From Chairside.Treatment where TREATMENT_ID = (Select TREATMENT_ID from CHAIRSIDE.HD_ORDER  where OCCURANCE_ID ="+"'"+elem[i-1]+"')))")
			while(rs.next()){

				if(rs.getString('DESCRIPTION')=='CLOSED'){
					System.out.println("Active")
				}


				else{



					Thread.sleep(60000)
					if(rs.getString('DESCRIPTION')=='CLOSED'){
						System.out.println("Passed")
					}
					else{
						System.out.println("Failed");

					}


				}
			}
			connection.close();




		}

	}
	@Keyword
	public void ihubshiftStatusCheck(String conn,String username,String password,String driverClassName){

		Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C:\\Users\\z00467714\\Documents\\DBExcel.xlsx', 'Data',true)
		int i;

		int rowCount=excelData.getRowNumbers()
		println(rowCount)
		for(i=1;i<=rowCount;i++){
			List elem=new ArrayList()
			elem[i-1] = excelData.getValue("OccID", i)


			println (elem[i-1])

			Class.forName(driverClassName);
			Properties props = new Properties();
			props.setProperty("user",username);
			props.setProperty("password",password);
			props.setProperty("ssl","true");
			Connection connection = DriverManager.getConnection(conn,props)
			Statement stmt=connection.createStatement();

			ResultSet rs=stmt.executeQuery("SELECT CS_STATUS_CODE FROM CDS.HEMO_ORDER_OCCURRENCE WHERE SC_ORDER_OCCURRENCE_ID="+"'"+elem[i-1]+"'")
			while(rs.next()){

				if(rs.getString('CS_STATUS_CODE')=='CM'){
					System.out.println("Completed")
				}


				else{
					int j;


					Thread.sleep(600000)
					if(rs.getString('CS_STATUS_CODE')=='CM'){
						System.out.println("Passed")
					}
					else{
						System.out.println("Failed");

					}


				}


			}

			connection.close();
		}



	}


}
