import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import excelHandle.excelGetValues as excelGetValues

Properties prop = supportingfiles.SupportingFiles.importPropertiesFile('Machine_Setup')

Properties prop1 = supportingfiles.SupportingFiles.importPropertiesFile('Environmental_Variables')

Properties  staticvalues = supportingfiles.SupportingFiles.importPropertiesFile('CS_EnvVariables')

//Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\MachineSetup.properties')

//Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')
//Properties  staticvalues = helperUtilities.getobject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\CS_EnvVariables.properties')
WebUI.delay(3)
println Username
WebUI.takeScreenshot()

WebDriver driver = DriverFactory.getWebDriver()
JavascriptExecutor js = (JavascriptExecutor) driver
WebElement setup = driver.findElement(By.xpath(prop.getProperty('MachineSetup_Update')))
println setup.getText()
js.executeScript("arguments[0].click()", setup)

//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('MachineSetup_Update'),'', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Button'), '', 'defaultcontent')
WebUI.delay(2)


println(prop.getProperty('Machine_number'))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Machine_number'),staticvalues.getProperty("MachineNo"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Treatment_section'),staticvalues.getProperty("Treatment_section"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Machine_Type'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Machine_Conductivity'),staticvalues.getProperty("Machine_Conductivity"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Meter_Conductivity'),staticvalues.getProperty("Meter_Conductivity"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Machine_Temp'),staticvalues.getProperty("Machine_Temp"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ph'),staticvalues.getProperty("ph_No"), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('NVL'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Alarms'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PHT'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AirDetectorArmed'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('HighFlux'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Bleach_NA'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PerformedBy'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.CS'(Username)
//CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty("Username"),prop1.getProperty("Password"))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Performed_Continue'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Update'), '', 'switchframe')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Button_Ok'), '', 'click')
WebUI.takeScreenshot()

WebDriver driver1 = DriverFactory.getWebDriver()
JavascriptExecutor js1 = (JavascriptExecutor) driver1
WebElement setup1 = driver1.findElement(By.xpath(prop.getProperty('MachineSetup_Update')))

js1.executeScript("arguments[0].click()", setup1)
println setup1.getText()

//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('MachineSetup_Update'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Button'), '', 'defaultcontent')
WebUI.delay(5)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ReviewedBy'), '', 'click')
WebUI.waitForPageLoad(5)

CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty("ReviewedUser"))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Performed_Continue'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Update'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Button_Ok'), '', 'click')
WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.delay(2)


