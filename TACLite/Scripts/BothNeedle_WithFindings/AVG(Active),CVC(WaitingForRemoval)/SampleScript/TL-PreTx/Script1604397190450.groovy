
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import com.kms.katalon.core.testobject.ConditionType as ConditionType

import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = supportingfiles.SupportingFiles.importPropertiesFile('Pre_Treatment')

Properties prop1 = supportingfiles.SupportingFiles.importPropertiesFile('Environmental_Variables')

//Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')

//Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')


CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'scrolltoelement')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'click')
WebUI.delay(3)

TestObject testObj = new TestObject()

def a = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Closebtn'))
WebUI.comment("Check for appointment reminders and click on close button")

if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)) {
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Closebtn'), '', 'click')
} else {
	println('Proceed to next-do nothing')
}


CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select1'), '', 'click')

WebUI.comment("Check if post-surgical assessment is present")

WebUI.callTestCase(findTestCase('CommonFunctions/PostSurgical'), [:], FailureHandling.OPTIONAL)

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_Findings', true)
def PreTx_Look1=excelData.getValue('PreTx_Look1', 2)
println(PreTx_Look1)

def TacUser=excelData.getValue('TAC User', 2)

String[] arr = PreTx_Look1.split(",")
for(String x:arr){
	println(''+x)
}
String a1=arr[0]
String b1=arr[1]
String c1=arr[2]
String k1=arr[3]
String l1=arr[4]
String m1=arr[5]
String n1=arr[6]
String o1=arr[7]

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+a1+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+b1+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+c1+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+k1+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+l1+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+m1+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+n1+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+o1+prop.getProperty('Pre'), '', 'click')


def PreTx_Listen1=excelData.getValue('PreTx_Listen1', 2)

String[] arr1 = PreTx_Listen1.split(",")
for(String y:arr1){
	println(''+y)
}
String d2=arr1[0]
String e2=arr1[1]


CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+d2+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+e2+prop.getProperty('Pre'), '', 'click')
WebUI.takeScreenshot()
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreOther'), '', 'scrolltoelement')

WebUI.delay(3)

def PreTx_Feel1=excelData.getValue('PreTx_Feel1', 2)

String[] arr2 = PreTx_Feel1.split(",")
for(String z:arr2){
	println(''+z)
}
String f3=arr2[0]
String g3=arr2[1]
String h3=arr2[2]
println(PreTx_Feel1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+f3+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+g3+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+h3+prop.getProperty('Pre'), '', 'click')

def PreTx_Other1=excelData.getValue('PreTx_Other1', 2)

String[] arr3 = PreTx_Other1.split(",")
for(String e:arr3){
	println(''+e)
}
String c4=arr3[0]
String d4=arr3[1]
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+c4+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+d4+prop.getProperty('Pre'), '', 'click')

WebUI.takeScreenshot()

CustomKeywords.'reusablekeyword.Reusable.TACSignature'(TacUser)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select2'), '', 'click')


WebUI.delay(3)
WebUI.takeScreenshot()
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('cvcyesbutton'), '', 'click')

def PreTx_Other2=excelData.getValue('PreTx_Other2', 3)
println(''+PreTx_Other2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+PreTx_Other2+prop.getProperty('Pre'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()
CustomKeywords.'reusablekeyword.Reusable.TACSignature'(TacUser)
WebUI.delay(2)
WebUI.takeScreenshot()





