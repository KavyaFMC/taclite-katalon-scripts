import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType

Properties prop = supportingfiles.SupportingFiles.importPropertiesFile('Post_Dialysis')

Properties prop1 = supportingfiles.SupportingFiles.importPropertiesFile('Environmental_Variables')

Properties  staticvalues = supportingfiles.SupportingFiles.importPropertiesFile('CS_EnvVariables')

//Properties  staticvalues = helperUtilities.getobject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\CS_EnvVariables.properties')

//Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\PostDialysis.properties')

//Properties prop1 = helperUtilities.getTheObj('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\EnvironmentalVariables.properties')

TestObject testObj=new TestObject()

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')

def cam=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('CAM'))

if(WebUI.waitForElementPresent(cam, 2, FailureHandling.OPTIONAL)) {

	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('CAM'), '', 'scrolltoelement')
	WebUI.takeScreenshot()
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('CAM'), '', 'click')
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
	WebUI.delay(2)
	
	def b=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Clinic_Check'))
	println(b)
	if (WebUI.waitForElementPresent(b, 3, FailureHandling.OPTIONAL)){
		
		CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Clinic_Check'), '', 'click')
		
		CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Clinic_Accept'), '', 'click')
		WebUI.takeScreenshot()
		
		CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'),'','click')
		WebUI.delay(5)
		CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty("Username"),prop1.getProperty("Password"))
		
		CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Postdialysis_Continue'), '', 'click')
		
		//CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
		WebUI.delay(2)
	}
	else{
		println("No Clinical Action")
	}
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Back'), '', 'click')
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Update'), '', 'switchframe')
	
	WebUI.delay(2)
	
	WebUI.comment("Check if button is present")
	def e=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Button_Ok'))
	
	if (WebUI.waitForElementPresent(e, 3, FailureHandling.OPTIONAL)){
		CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Button_Ok'), '', 'click')
	}
	else{
		println("Proceed to next-do nothing")
	}
}else {
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Update'), '', 'switchframe')
}

