import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.io.IOException
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testdata.InternalData
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

import java.lang.CharSequence as CharSequence
import java.sql.Driver
import java.util.concurrent.TimeUnit

import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.lang.CharSequence as CharSequence
import com.kms.katalon.core.testobject.ConditionType
import utilityKeywords.helperUtilities as helperUtilities

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFRow

/*Properties prop = helperUtilities.getTheObject('C://Katalon//NxStage Orders//Data Resources//NxStageOrders//NxStage_MachineSetup.properties')
Properties prop1 = helperUtilities.getTheObj('C://Katalon//NxStage Orders//Data Resources//ChairSide//CS_PatientSelection.properties')
Properties prop2 = helperUtilities.getObj('C://Katalon//NxStage Orders//Data Resources//NxStageOrders//NxStage_PropertyValues.properties')
Properties prop3 = helperUtilities.getObj1('C://Katalon//NxStage Orders//Data Resources//NxStageOrders//NxStage_EnvVariables.properties')
Properties prop4 = helperUtilities.getObj2('C://Katalon//NxStage Orders//Data Resources/ChairSide//CS_StaticValues.properties')
Properties prop5 = helperUtilities.getObj3('C://Katalon//NxStage Orders//Data Resources/ChairSide//CS_PreDialysis.properties')
*/
clearValues("C:\\katalon\\TACLite\\InputData\\ReadWrite.xlsx")
clearValues1("C:\\katalon\\TACLite\\InputData\\ReadWrite.xlsx")

public clearValues(String ExcelPath){
//	"C:\\Katalon\\NxStage Orders\\InputData\\NxStageTestData.xls"
	FileInputStream fis = new FileInputStream(ExcelPath);
	XSSFWorkbook workbook = new XSSFWorkbook(fis);
	XSSFSheet sheet = workbook.getSheet("Data");
	int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
	println(rowCount)
for(int i = 1; i <= rowCount; i++)
{
	
	Row row = sheet.getRow(i);
	println(i)
	sheet.removeRow(row);
	//println(sheet.getRow(i))
	
	
}



FileOutputStream out = new FileOutputStream(ExcelPath);
workbook.write(out)
out.close();
}

public clearValues1(String ExcelPath){
	//	"C:\\Katalon\\NxStage Orders\\InputData\\NxStageTestData.xls"
		FileInputStream fis = new FileInputStream(ExcelPath);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet("Data1");
		int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
		println(rowCount)
	for(int i = 1; i <= rowCount; i++)
	{
		
		Row row = sheet.getRow(i);
		println(i)
		sheet.removeRow(row);
		//println(sheet.getRow(i))
		
		
	}
	

	
	FileOutputStream out = new FileOutputStream(ExcelPath);
	workbook.write(out)
	out.close();
	}
