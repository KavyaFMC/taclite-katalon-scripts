import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'SingleNeedle_NoFindings', true)

def User=excelData1.getValue('CS User', 1)

WebUI.callTestCase(findTestCase('CommonFunctions/clearValues'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/SampleScripts/CS_PatientSelection'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CommonFunctions/CAM alert'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleNeedle_No UsualFindings/ChairSideScripts/CS_MachineSetup'), [("Username"):User], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleNeedle_No UsualFindings/ChairSideScripts/CS_PreDialysis'), [("Username"):User], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/SampleScripts/TL-AddAccess'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/SampleScripts/TL-PreTx'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/SampleScripts/CS_StartTx'),
	[:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/SampleScripts/TL-CannDoc'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/SampleScripts/TL_AFT'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/SampleScripts/TL-DuringPost'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/SampleScripts/CS_PostDialysis'), 
    [:], FailureHandling.STOP_ON_FAILURE)



